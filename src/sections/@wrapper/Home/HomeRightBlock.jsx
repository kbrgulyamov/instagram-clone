import React from "react";
import { Box, Typography } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import img from "../../../img/2.jpg";

const HomeRightBlock = () => {
  return (
    <Box position={"fixed"}>
      <Box bgcolor="#fafafa" width="320px" height="300vh">
        <Box p="20px ">
          <Box display={"flex"} gap="10px">
            <Avatar
              alt="Remy Sharp"
              src="https://instagram.ftas1-2.fna.fbcdn.net/v/t51.2885-19/294500537_707078950392792_638766400944081269_n.jpg?stp=dst-jpg_s320x320&_nc_ht=instagram.ftas1-2.fna.fbcdn.net&_nc_cat=100&_nc_ohc=3V6pZgH1uhAAX9IrukS&edm=AOQ1c0wBAAAA&ccb=7-5&oh=00_AT849CdWebTmXwG_VB-LAaRrW3VF4QCC3IEEEZnlUKPHZw&oe=6308BEBA&_nc_sid=8fd12b"
              sx={{ width: 56, height: 56 }}
            />
            <Box display={"flex"} flexDirection="column" pt={0.9}>
              <Typography>Remy Sharp</Typography>
              <Typography fontSize="12px">🚀CEO Founder of Zero Full Stack </Typography>
            </Box>
          </Box>
          <Box pt="26px" mb={2}>
            <Typography fontSize="18px" color="#606770" fontWeight="800">
              Suggestions for you
            </Typography>
          </Box>
          <Box display={"flex"} flexDirection="column" gap="20px">
            <Box display={"flex"} gap="10px">
              <Avatar alt="Remy Sharp" src={img} sx={{ width: 49, height: 49 }} />
              <Box display={"flex"} flexDirection="column">
                <Typography>Remy Sharp</Typography>
                <Typography fontSize="12px">🚀CEO Founder of Zero Full Stack </Typography>
              </Box>
            </Box>
            <Box display={"flex"} gap="10px">
              <Avatar alt="Remy Sharp" src={img} sx={{ width: 49, height: 49 }} />
              <Box display={"flex"} flexDirection="column">
                <Typography>Remy Sharp</Typography>
                <Typography fontSize="12px">🚀CEO Founder of Zero Full Stack </Typography>
              </Box>
            </Box>
            <Box display={"flex"} gap="10px">
              <Avatar alt="Remy Sharp" src={img} sx={{ width: 49, height: 49 }} />
              <Box display={"flex"} flexDirection="column">
                <Typography>Remy Sharp</Typography>
                <Typography fontSize="12px">🚀CEO Founder of Zero Full Stack </Typography>
              </Box>
            </Box>
            <Box display={"flex"} gap="10px">
              <Avatar alt="Remy Sharp" src={img} sx={{ width: 49, height: 49 }} />
              <Box display={"flex"} flexDirection="column">
                <Typography>Remy Sharp</Typography>
                <Typography fontSize="12px">🚀CEO Founder of Zero Full Stack </Typography>
              </Box>
            </Box>
            <Box display={"flex"} gap="10px">
              <Avatar alt="Remy Sharp" src={img} sx={{ width: 49, height: 49 }} />
              <Box display={"flex"} flexDirection="column">
                <Typography>Remy Sharp</Typography>
                <Typography fontSize="12px">🚀CEO Founder of Zero Full Stack </Typography>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default HomeRightBlock;
