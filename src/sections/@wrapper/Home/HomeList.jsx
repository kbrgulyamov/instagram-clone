// import React, { useState } from "react";
// import { Box } from "@mui/material";
// import { useRef } from "react";
// import useScroll from "../../../Core/hook/useScroll";

// const HomeList = () => {
//   const [users, setUsers] = useState([]);
//   const [page, setPage] = useState(1);
//   const limit = 4;
//   const parentRef = useRef();
//   const childRef = useRef();

//   const intersected = useScroll(parentRef, childRef, () => fetchUsers(page, limit));

//   function fetchUsers(page, limit) {
//     fetch(`https://jsonplaceholder.typicode.com/users?_limit=${limit}&_page=${page}`)
//       .then((response) => response.json())
//       .then((json) => {
//         setUsers((prev) => [...prev, ...json]);
//         setPage((prev) => prev + 1);
//       });
//   }

//   return (
//     <Box
//       ref={parentRef}
//       display={"flex"}
//       flexDirection="column"
//       gap={5}
//       justifyContent="center"
//       height="80vh"
//     >
//       {users.map((i) => (
//         <Box key={i.id} width="600px" height="200px" bgcolor="#fff">
//           {i.id}. {i.name}
//         </Box>
//       ))}
//       <Box height="20px" bgcolor="blue" />
//     </Box>
//   );
// };

// export default HomeList;

import React, { useRef, useState } from "react";
import useScroll from "../../../Core/hook/useScroll";
import { Box } from "@mui/material";

const List = () => {
  const [todos, setTodos] = useState([]);
  const [page, setPage] = useState(1);
  const limit = 5;
  const parentRef = useRef();
  const childRef = useRef();
  const intersected = useScroll(parentRef, childRef, () => fetchTodos(page, limit));

  function fetchTodos(page, limit) {
    fetch(`https://jsonplaceholder.typicode.com/todos?_limit=${limit}&_page=${page}`)
      .then((response) => response.json())
      .then((json) => {
        setTodos((prev) => [...prev, ...json]);
        setPage((prev) => prev + 1);
      });
  }

  return (
    <Box
      ref={parentRef}
      display={"flex"}
      flexDirection="column"
      gap={5}
      height="80vh"
      sx={{ overflow: "auto" }}
    >
      {todos.map((todo) => (
        <Box key={todo.id} width="600px" height="200px" bgcolor="#fff">
          {todo.id}. {todo.title}
        </Box>
      ))}
      <div ref={childRef}>
        <h3 style={{ textAlign: "center" }}>Loading.....</h3>
      </div>
    </Box>
  );
};

export default List;
