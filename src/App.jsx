import Routers from "./Core/Routes/routes";
import "./App.css";

function App() {
  return (
    <>
      <Routers />
    </>
  );
}

export default App;
