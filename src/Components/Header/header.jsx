import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import CssBaseline from "@mui/material/CssBaseline";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Fab from "@mui/material/Fab";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Fade from "@mui/material/Fade";
import AccountMenu from "./menuHeader";
import Logo from "./img/Instagram-Wordmark-Black-Logo.wine.svg";
import { Link } from "react-router-dom";

function ScrollTop(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector("#back-to-top-anchor");

    if (anchor) {
      anchor.scrollIntoView({
        block: "center",
      });
    }
  };

  return (
    <Fade in={trigger}>
      <Box
        onClick={handleClick}
        role="presentation"
        sx={{ position: "fixed", bottom: 16, right: 16 }}
      >
        {children}
      </Box>
    </Fade>
  );
}

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default function BackToTop(props) {
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar
        sx={{
          height: "70px",
          bgcolor: "#fff",
          color: "#1c1e21",
          boxShadow: "none",
          border: "1px solid #cecece",
        }}
      >
        <Container>
          <Box display={"flex"} justifyContent="space-between" pt={1.8}>
            <Box mt={-3.2}>
              <Link to={"/home"}>
                <img style={{ width: "150px" }} src={Logo} alt="" />
              </Link>
            </Box>
            <Box display={"flex"} justifyContent="center">
              <Box display={"flex"}>
                <Box
                  sx={{
                    position: "relative",
                    left: "27px",
                    top: "9px",
                    color: "rgb(142, 142, 142)",
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="1em"
                    height="1em"
                    preserveAspectRatio="xMidYMid meet"
                    viewBox="0 0 24 24"
                  >
                    <path
                      fill="none"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-width="2"
                      d="m21 21l-4.486-4.494M19 10.5a8.5 8.5 0 1 1-17 0a8.5 8.5 0 0 1 17 0Z"
                    />
                  </svg>
                </Box>
                <input
                  style={{
                    height: "40px",
                    width: "300px",
                    backgroundColor: "#fafafa",
                    border: "1px solid #f4f4f4",
                    borderRadius: "2px",
                    padding: "10px 35px",
                    outline: "none",
                  }}
                  placeholder="Search"
                />
              </Box>
            </Box>
            <Box>
              <AccountMenu />
            </Box>
          </Box>
        </Container>
      </AppBar>
      <Toolbar id="back-to-top-anchor" />

      <ScrollTop {...props}>
        <Fab size="small" aria-label="scroll back to top">
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </React.Fragment>
  );
}
