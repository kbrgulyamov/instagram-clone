import React from "react";
import { Container, Box } from "@mui/material";
import BackToTop from "../Components/Header/header";
import HomeRightBlock from "../sections/@wrapper/Home/HomeRightBlock";
import List from "../sections/@wrapper/Home/HomeList";
import { Helmet } from "react-helmet";

// import HomeList from "../sections/@wrapper/Home/HomeList";

export default function Home() {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Home</title>
      </Helmet>
      <BackToTop />
      <Container sx={{ marginTop: 5.5 }}>
        <Box display={"flex"} justifyContent="end">
          <HomeRightBlock />
        </Box>
        <Box ml={19}>
          {/* <HomeList /> */}
          <List />
        </Box>
      </Container>
    </>
  );
}
